<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.6.3</string>
        <key>fileName</key>
        <string>/Users/Jackie/WorkSpace/CasinoPlatform/graphics/deck_atlas.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>1024</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>cards/deck_atlas.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <true/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">cards/src/2c.png</key>
            <key type="filename">cards/src/2d.png</key>
            <key type="filename">cards/src/2h.png</key>
            <key type="filename">cards/src/2s.png</key>
            <key type="filename">cards/src/3c.png</key>
            <key type="filename">cards/src/3d.png</key>
            <key type="filename">cards/src/3h.png</key>
            <key type="filename">cards/src/3s.png</key>
            <key type="filename">cards/src/4c.png</key>
            <key type="filename">cards/src/4d.png</key>
            <key type="filename">cards/src/4h.png</key>
            <key type="filename">cards/src/4s.png</key>
            <key type="filename">cards/src/5c.png</key>
            <key type="filename">cards/src/5d.png</key>
            <key type="filename">cards/src/5h.png</key>
            <key type="filename">cards/src/5s.png</key>
            <key type="filename">cards/src/6c.png</key>
            <key type="filename">cards/src/6d.png</key>
            <key type="filename">cards/src/6h.png</key>
            <key type="filename">cards/src/6s.png</key>
            <key type="filename">cards/src/7c.png</key>
            <key type="filename">cards/src/7d.png</key>
            <key type="filename">cards/src/7h.png</key>
            <key type="filename">cards/src/7s.png</key>
            <key type="filename">cards/src/8c.png</key>
            <key type="filename">cards/src/8d.png</key>
            <key type="filename">cards/src/8h.png</key>
            <key type="filename">cards/src/8s.png</key>
            <key type="filename">cards/src/9c.png</key>
            <key type="filename">cards/src/9d.png</key>
            <key type="filename">cards/src/9h.png</key>
            <key type="filename">cards/src/9s.png</key>
            <key type="filename">cards/src/Ac.png</key>
            <key type="filename">cards/src/Ad.png</key>
            <key type="filename">cards/src/Ah.png</key>
            <key type="filename">cards/src/As.png</key>
            <key type="filename">cards/src/Jc.png</key>
            <key type="filename">cards/src/Jd.png</key>
            <key type="filename">cards/src/Jh.png</key>
            <key type="filename">cards/src/Js.png</key>
            <key type="filename">cards/src/Kc.png</key>
            <key type="filename">cards/src/Kd.png</key>
            <key type="filename">cards/src/Kh.png</key>
            <key type="filename">cards/src/Ks.png</key>
            <key type="filename">cards/src/Qc.png</key>
            <key type="filename">cards/src/Qd.png</key>
            <key type="filename">cards/src/Qh.png</key>
            <key type="filename">cards/src/Qs.png</key>
            <key type="filename">cards/src/Tc.png</key>
            <key type="filename">cards/src/Td.png</key>
            <key type="filename">cards/src/Th.png</key>
            <key type="filename">cards/src/Ts.png</key>
            <key type="filename">cards/src/back_blue.png</key>
            <key type="filename">cards/src/back_red.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>42,56,84,112</rect>
                <key>scale9Paddings</key>
                <rect>42,56,84,112</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>cards/src/2c.png</filename>
            <filename>cards/src/2d.png</filename>
            <filename>cards/src/2h.png</filename>
            <filename>cards/src/2s.png</filename>
            <filename>cards/src/3c.png</filename>
            <filename>cards/src/3d.png</filename>
            <filename>cards/src/3h.png</filename>
            <filename>cards/src/3s.png</filename>
            <filename>cards/src/4c.png</filename>
            <filename>cards/src/4d.png</filename>
            <filename>cards/src/4h.png</filename>
            <filename>cards/src/4s.png</filename>
            <filename>cards/src/5c.png</filename>
            <filename>cards/src/5d.png</filename>
            <filename>cards/src/5h.png</filename>
            <filename>cards/src/5s.png</filename>
            <filename>cards/src/6c.png</filename>
            <filename>cards/src/6d.png</filename>
            <filename>cards/src/6h.png</filename>
            <filename>cards/src/6s.png</filename>
            <filename>cards/src/7c.png</filename>
            <filename>cards/src/7d.png</filename>
            <filename>cards/src/7h.png</filename>
            <filename>cards/src/7s.png</filename>
            <filename>cards/src/8c.png</filename>
            <filename>cards/src/8d.png</filename>
            <filename>cards/src/8h.png</filename>
            <filename>cards/src/8s.png</filename>
            <filename>cards/src/9c.png</filename>
            <filename>cards/src/9d.png</filename>
            <filename>cards/src/9h.png</filename>
            <filename>cards/src/9s.png</filename>
            <filename>cards/src/Ac.png</filename>
            <filename>cards/src/Ad.png</filename>
            <filename>cards/src/Ah.png</filename>
            <filename>cards/src/As.png</filename>
            <filename>cards/src/back_blue.png</filename>
            <filename>cards/src/back_red.png</filename>
            <filename>cards/src/Jc.png</filename>
            <filename>cards/src/Jd.png</filename>
            <filename>cards/src/Jh.png</filename>
            <filename>cards/src/Js.png</filename>
            <filename>cards/src/Kc.png</filename>
            <filename>cards/src/Kd.png</filename>
            <filename>cards/src/Kh.png</filename>
            <filename>cards/src/Ks.png</filename>
            <filename>cards/src/Qc.png</filename>
            <filename>cards/src/Qd.png</filename>
            <filename>cards/src/Qh.png</filename>
            <filename>cards/src/Qs.png</filename>
            <filename>cards/src/Tc.png</filename>
            <filename>cards/src/Td.png</filename>
            <filename>cards/src/Th.png</filename>
            <filename>cards/src/Ts.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
