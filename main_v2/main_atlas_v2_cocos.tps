<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.6.3</string>
        <key>fileName</key>
        <string>/Users/Jackie/WorkSpace/CasinoPlatform/graphics/main_v2/main_atlas_v2_cocos.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>cocos2d-x</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>5</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>1024</int>
            <key>height</key>
            <int>1024</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">LongSideFit</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>main_atlas_cocos.plist</filename>
            </struct>
            <key>header</key>
            <key>source</key>
            <struct type="DataFile">
                <key>name</key>
                <filename></filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">KeepTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <true/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>99</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../chips/chip_0.png</key>
            <key type="filename">../chips/chip_1.png</key>
            <key type="filename">../chips/chip_10.png</key>
            <key type="filename">../chips/chip_100.png</key>
            <key type="filename">../chips/chip_1000.png</key>
            <key type="filename">../chips/chip_10000.png</key>
            <key type="filename">../chips/chip_100000.png</key>
            <key type="filename">../chips/chip_1000000.png</key>
            <key type="filename">../chips/chip_2.png</key>
            <key type="filename">../chips/chip_20.png</key>
            <key type="filename">../chips/chip_2000.png</key>
            <key type="filename">../chips/chip_20000.png</key>
            <key type="filename">../chips/chip_25.png</key>
            <key type="filename">../chips/chip_250.png</key>
            <key type="filename">../chips/chip_5.png</key>
            <key type="filename">../chips/chip_50.png</key>
            <key type="filename">../chips/chip_500.png</key>
            <key type="filename">../chips/chip_5000.png</key>
            <key type="filename">../chips/chip_50000.png</key>
            <key type="filename">src/tx_progressbar_background.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,10,20,20</rect>
                <key>scale9Paddings</key>
                <rect>10,10,20,20</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_arrow_background.png</key>
            <key type="filename">src/tx_arrowi_background.png</key>
            <key type="filename">src/tx_button_circle_black_small.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,15,30,30</rect>
                <key>scale9Paddings</key>
                <rect>15,15,30,30</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_avatar_mask.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>36,35,71,71</rect>
                <key>scale9Paddings</key>
                <rect>36,35,71,71</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_background.png</key>
            <key type="filename">src/tx_progressbar_frontground.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_bar_blue.png</key>
            <key type="filename">src/tx_button_blue.png</key>
            <key type="filename">src/tx_button_green.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,18,35,35</rect>
                <key>scale9Paddings</key>
                <rect>18,18,35,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_bar_bottom.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>120,30,240,60</rect>
                <key>scale9Paddings</key>
                <rect>120,30,240,60</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_bar_top.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>240,35,480,69</rect>
                <key>scale9Paddings</key>
                <rect>240,35,480,69</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_bar_vertical_blue.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>1,17,1,34</rect>
                <key>scale9Paddings</key>
                <rect>1,17,1,34</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_box.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,20,39,39</rect>
                <key>scale9Paddings</key>
                <rect>20,20,39,39</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_button_back.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,20,40,40</rect>
                <key>scale9Paddings</key>
                <rect>20,20,40,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_button_blue_shadow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>17,25,35,51</rect>
                <key>scale9Paddings</key>
                <rect>17,25,35,51</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_button_challenge.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>65,43,131,85</rect>
                <key>scale9Paddings</key>
                <rect>65,43,131,85</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_button_circle.png</key>
            <key type="filename">src/tx_button_circle_disabled.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,25,51,51</rect>
                <key>scale9Paddings</key>
                <rect>25,25,51,51</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_button_circle_black.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>40,40,81,81</rect>
                <key>scale9Paddings</key>
                <rect>40,40,81,81</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_button_green_shadow.png</key>
            <key type="filename">src/tx_button_yellow_shadow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,23,35,45</rect>
                <key>scale9Paddings</key>
                <rect>18,23,35,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_button_help.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,10,20,21</rect>
                <key>scale9Paddings</key>
                <rect>10,10,20,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_button_mid_blue.png</key>
            <key type="filename">src/tx_button_side_blue.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,18,49,36</rect>
                <key>scale9Paddings</key>
                <rect>25,18,49,36</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_button_orange_shadow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,25,35,51</rect>
                <key>scale9Paddings</key>
                <rect>18,25,35,51</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_chip_blue.png</key>
            <key type="filename">src/tx_chip_green.png</key>
            <key type="filename">src/tx_chip_orange.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,34,70,67</rect>
                <key>scale9Paddings</key>
                <rect>35,34,70,67</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_icon_back.png</key>
            <key type="filename">src/tx_icon_bell.png</key>
            <key type="filename">src/tx_icon_clock.png</key>
            <key type="filename">src/tx_icon_cross.png</key>
            <key type="filename">src/tx_icon_facebook.png</key>
            <key type="filename">src/tx_icon_mail.png</key>
            <key type="filename">src/tx_icon_next.png</key>
            <key type="filename">src/tx_icon_personal.png</key>
            <key type="filename">src/tx_icon_phone.png</key>
            <key type="filename">src/tx_icon_plus.png</key>
            <key type="filename">src/tx_icon_rank.png</key>
            <key type="filename">src/tx_icon_refresh.png</key>
            <key type="filename">src/tx_icon_search.png</key>
            <key type="filename">src/tx_icon_setting.png</key>
            <key type="filename">src/tx_icon_wallet.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,18,36,36</rect>
                <key>scale9Paddings</key>
                <rect>18,18,36,36</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_icon_challenge.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>19,16,38,32</rect>
                <key>scale9Paddings</key>
                <rect>19,16,38,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_icon_money.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,9,13,18</rect>
                <key>scale9Paddings</key>
                <rect>7,9,13,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_icon_sort.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,5,15,10</rect>
                <key>scale9Paddings</key>
                <rect>8,5,15,10</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_icon_vip_01.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,16,49,31</rect>
                <key>scale9Paddings</key>
                <rect>24,16,49,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_inputfield.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>175,20,350,40</rect>
                <key>scale9Paddings</key>
                <rect>175,20,350,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_loading_spinner.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,14,28,28</rect>
                <key>scale9Paddings</key>
                <rect>14,14,28,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_rank_bronze.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,12,24,24</rect>
                <key>scale9Paddings</key>
                <rect>12,12,24,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_rank_title.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>74,16,149,31</rect>
                <key>scale9Paddings</key>
                <rect>74,16,149,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_red_dot.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,13,25,26</rect>
                <key>scale9Paddings</key>
                <rect>13,13,25,26</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_toggle_off.png</key>
            <key type="filename">src/tx_toggle_on.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>53,18,105,35</rect>
                <key>scale9Paddings</key>
                <rect>53,18,105,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_top_background.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>34,34,67,67</rect>
                <key>scale9Paddings</key>
                <rect>34,34,67,67</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_top_jackpot.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>185,34,370,69</rect>
                <key>scale9Paddings</key>
                <rect>185,34,370,69</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_top_jackpot_cell.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,19,21,37</rect>
                <key>scale9Paddings</key>
                <rect>11,19,21,37</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_white_rect.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,8,8</rect>
                <key>scale9Paddings</key>
                <rect>4,4,8,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>src/tx_button_circle_disabled.png</filename>
            <filename>src/tx_button_circle.png</filename>
            <filename>src/tx_icon_money.png</filename>
            <filename>src/tx_icon_setting.png</filename>
            <filename>src/tx_icon_wallet.png</filename>
            <filename>src/tx_progressbar_background.png</filename>
            <filename>src/tx_progressbar_frontground.png</filename>
            <filename>src/tx_avatar_mask.png</filename>
            <filename>src/tx_background.png</filename>
            <filename>src/tx_bar_blue.png</filename>
            <filename>src/tx_bar_bottom.png</filename>
            <filename>src/tx_bar_top.png</filename>
            <filename>src/tx_bar_vertical_blue.png</filename>
            <filename>src/tx_box.png</filename>
            <filename>src/tx_button_back.png</filename>
            <filename>src/tx_button_blue_shadow.png</filename>
            <filename>src/tx_button_blue.png</filename>
            <filename>src/tx_button_circle_black_small.png</filename>
            <filename>src/tx_button_circle_black.png</filename>
            <filename>src/tx_button_green_shadow.png</filename>
            <filename>src/tx_button_green.png</filename>
            <filename>src/tx_button_mid_blue.png</filename>
            <filename>src/tx_button_orange_shadow.png</filename>
            <filename>src/tx_button_side_blue.png</filename>
            <filename>src/tx_button_yellow_shadow.png</filename>
            <filename>src/tx_icon_back.png</filename>
            <filename>src/tx_icon_bell.png</filename>
            <filename>src/tx_icon_clock.png</filename>
            <filename>src/tx_icon_cross.png</filename>
            <filename>src/tx_icon_facebook.png</filename>
            <filename>src/tx_icon_mail.png</filename>
            <filename>src/tx_icon_next.png</filename>
            <filename>src/tx_icon_personal.png</filename>
            <filename>src/tx_icon_phone.png</filename>
            <filename>src/tx_icon_plus.png</filename>
            <filename>src/tx_icon_rank.png</filename>
            <filename>src/tx_icon_refresh.png</filename>
            <filename>src/tx_icon_search.png</filename>
            <filename>src/tx_icon_sort.png</filename>
            <filename>src/tx_inputfield.png</filename>
            <filename>src/tx_red_dot.png</filename>
            <filename>src/tx_toggle_off.png</filename>
            <filename>src/tx_toggle_on.png</filename>
            <filename>../chips/chip_0.png</filename>
            <filename>../chips/chip_1.png</filename>
            <filename>../chips/chip_2.png</filename>
            <filename>../chips/chip_5.png</filename>
            <filename>../chips/chip_10.png</filename>
            <filename>../chips/chip_20.png</filename>
            <filename>../chips/chip_25.png</filename>
            <filename>../chips/chip_50.png</filename>
            <filename>../chips/chip_100.png</filename>
            <filename>../chips/chip_250.png</filename>
            <filename>../chips/chip_500.png</filename>
            <filename>../chips/chip_1000.png</filename>
            <filename>../chips/chip_2000.png</filename>
            <filename>../chips/chip_5000.png</filename>
            <filename>../chips/chip_10000.png</filename>
            <filename>../chips/chip_20000.png</filename>
            <filename>../chips/chip_50000.png</filename>
            <filename>../chips/chip_100000.png</filename>
            <filename>../chips/chip_1000000.png</filename>
            <filename>src/tx_top_background.png</filename>
            <filename>src/tx_top_jackpot_cell.png</filename>
            <filename>src/tx_top_jackpot.png</filename>
            <filename>src/tx_loading_spinner.png</filename>
            <filename>src/tx_white_rect.png</filename>
            <filename>src/tx_arrow_background.png</filename>
            <filename>src/tx_arrowi_background.png</filename>
            <filename>src/tx_button_help.png</filename>
            <filename>src/tx_icon_vip_01.png</filename>
            <filename>src/tx_button_challenge.png</filename>
            <filename>src/tx_chip_blue.png</filename>
            <filename>src/tx_chip_green.png</filename>
            <filename>src/tx_chip_orange.png</filename>
            <filename>src/tx_icon_challenge.png</filename>
            <filename>src/tx_rank_bronze.png</filename>
            <filename>src/tx_rank_title.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
