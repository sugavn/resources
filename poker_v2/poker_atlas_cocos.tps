<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.6.3</string>
        <key>fileName</key>
        <string>/Users/Jackie/WorkSpace/CasinoPlatform/graphics/poker_v2/poker_atlas_cocos.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>cocos2d-x</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>5</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">AreaFit</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>poker_atlas_cocos.plist</filename>
            </struct>
            <key>header</key>
            <key>source</key>
            <struct type="DataFile">
                <key>name</key>
                <filename></filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">KeepTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <true/>
        <key>prependSmartFolderName</key>
        <true/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>2</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>38</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">src/tx_avatar_background.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,63,127,127</rect>
                <key>scale9Paddings</key>
                <rect>63,63,127,127</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_backgrond_overlay.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>240,135,480,271</rect>
                <key>scale9Paddings</key>
                <rect>240,135,480,271</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_background.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>75,75,150,150</rect>
                <key>scale9Paddings</key>
                <rect>75,75,150,150</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_bar_blue.png</key>
            <key type="filename">src/tx_button_top_round.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,18,35,35</rect>
                <key>scale9Paddings</key>
                <rect>18,18,35,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_bar_green.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,14,47,27</rect>
                <key>scale9Paddings</key>
                <rect>24,14,47,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_bar_vertical_blue.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>1,17,1,34</rect>
                <key>scale9Paddings</key>
                <rect>1,17,1,34</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_box.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,20,39,39</rect>
                <key>scale9Paddings</key>
                <rect>20,20,39,39</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>33,16,65,33</rect>
                <key>scale9Paddings</key>
                <rect>33,16,65,33</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_button_call.png</key>
            <key type="filename">src/tx_button_check.png</key>
            <key type="filename">src/tx_button_raise.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>103,23,205,45</rect>
                <key>scale9Paddings</key>
                <rect>103,23,205,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_button_circle.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,23,45,45</rect>
                <key>scale9Paddings</key>
                <rect>23,23,45,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_button_circle_small.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>17,17,35,35</rect>
                <key>scale9Paddings</key>
                <rect>17,17,35,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_button_circle_small_white.png</key>
            <key type="filename">src/tx_progress_bg.png</key>
            <key type="filename">src/tx_rounded_background.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,10,20,20</rect>
                <key>scale9Paddings</key>
                <rect>10,10,20,20</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_button_green_shadow.png</key>
            <key type="filename">src/tx_button_yellow_shadow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,23,35,45</rect>
                <key>scale9Paddings</key>
                <rect>18,23,35,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_button_help.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,10,20,21</rect>
                <key>scale9Paddings</key>
                <rect>10,10,20,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_button_mid_blue.png</key>
            <key type="filename">src/tx_button_side_blue.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,18,49,36</rect>
                <key>scale9Paddings</key>
                <rect>25,18,49,36</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_button_orange_shadow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,25,35,51</rect>
                <key>scale9Paddings</key>
                <rect>18,25,35,51</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_button_scroll.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,17,63,33</rect>
                <key>scale9Paddings</key>
                <rect>32,17,63,33</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_cashbox.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>75,20,149,40</rect>
                <key>scale9Paddings</key>
                <rect>75,20,149,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_chat_header.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>151,15,301,30</rect>
                <key>scale9Paddings</key>
                <rect>151,15,301,30</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_checkbox_box.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,15,30,30</rect>
                <key>scale9Paddings</key>
                <rect>15,15,30,30</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_checkbox_mark.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,13,25,25</rect>
                <key>scale9Paddings</key>
                <rect>13,13,25,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_circle_border_white.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>66,66,131,131</rect>
                <key>scale9Paddings</key>
                <rect>66,66,131,131</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_dealer.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>39,64,79,127</rect>
                <key>scale9Paddings</key>
                <rect>39,64,79,127</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_dealer_button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,13,26,26</rect>
                <key>scale9Paddings</key>
                <rect>13,13,26,26</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_icon_chat.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,8,20,17</rect>
                <key>scale9Paddings</key>
                <rect>10,8,20,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_icon_gift.png</key>
            <key type="filename">src/tx_icon_plus.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,15,15</rect>
                <key>scale9Paddings</key>
                <rect>8,8,15,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_inputfield.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>175,20,350,40</rect>
                <key>scale9Paddings</key>
                <rect>175,20,350,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_progress_blue_fg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,8,17,16</rect>
                <key>scale9Paddings</key>
                <rect>9,8,17,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_progress_fg.png</key>
            <key type="filename">src/tx_window_background.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_raise_background.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>28,28,55,55</rect>
                <key>scale9Paddings</key>
                <rect>28,28,55,55</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_raise_background_blue.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,225,7,449</rect>
                <key>scale9Paddings</key>
                <rect>4,225,7,449</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_red_dot.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,13,25,26</rect>
                <key>scale9Paddings</key>
                <rect>13,13,25,26</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_status_call.png</key>
            <key type="filename">src/tx_status_check.png</key>
            <key type="filename">src/tx_status_fold.png</key>
            <key type="filename">src/tx_status_raise.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>17,8,35,15</rect>
                <key>scale9Paddings</key>
                <rect>17,8,35,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_table.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>223,183,445,365</rect>
                <key>scale9Paddings</key>
                <rect>223,183,445,365</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_toggle_mark_bcyan.png</key>
            <key type="filename">src/tx_toggle_mark_green.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,14,9,28</rect>
                <key>scale9Paddings</key>
                <rect>4,14,9,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_toggle_mark_cyan_bg.png</key>
            <key type="filename">src/tx_toggle_mark_green_bg.png</key>
            <key type="filename">src/tx_toggle_mark_yellow_bg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,15,10,30</rect>
                <key>scale9Paddings</key>
                <rect>5,15,10,30</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_toggle_mark_yellow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,14,8,28</rect>
                <key>scale9Paddings</key>
                <rect>4,14,8,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">src/tx_txtbar.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>92,19,183,39</rect>
                <key>scale9Paddings</key>
                <rect>92,19,183,39</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>src/tx_avatar_background.png</filename>
            <filename>src/tx_backgrond_overlay.png</filename>
            <filename>src/tx_background.png</filename>
            <filename>src/tx_bar_blue.png</filename>
            <filename>src/tx_bar_green.png</filename>
            <filename>src/tx_bar_vertical_blue.png</filename>
            <filename>src/tx_box.png</filename>
            <filename>src/tx_button_call.png</filename>
            <filename>src/tx_button_check.png</filename>
            <filename>src/tx_button_circle_small_white.png</filename>
            <filename>src/tx_button_circle_small.png</filename>
            <filename>src/tx_button_circle.png</filename>
            <filename>src/tx_button_green_shadow.png</filename>
            <filename>src/tx_button_help.png</filename>
            <filename>src/tx_button_mid_blue.png</filename>
            <filename>src/tx_button_orange_shadow.png</filename>
            <filename>src/tx_button_raise.png</filename>
            <filename>src/tx_button_scroll.png</filename>
            <filename>src/tx_button_side_blue.png</filename>
            <filename>src/tx_button_top_round.png</filename>
            <filename>src/tx_button_yellow_shadow.png</filename>
            <filename>src/tx_button.png</filename>
            <filename>src/tx_cashbox.png</filename>
            <filename>src/tx_chat_header.png</filename>
            <filename>src/tx_checkbox_box.png</filename>
            <filename>src/tx_checkbox_mark.png</filename>
            <filename>src/tx_circle_border_white.png</filename>
            <filename>src/tx_dealer_button.png</filename>
            <filename>src/tx_dealer.png</filename>
            <filename>src/tx_icon_chat.png</filename>
            <filename>src/tx_icon_gift.png</filename>
            <filename>src/tx_icon_plus.png</filename>
            <filename>src/tx_inputfield.png</filename>
            <filename>src/tx_progress_bg.png</filename>
            <filename>src/tx_progress_blue_fg.png</filename>
            <filename>src/tx_progress_fg.png</filename>
            <filename>src/tx_raise_background_blue.png</filename>
            <filename>src/tx_raise_background.png</filename>
            <filename>src/tx_red_dot.png</filename>
            <filename>src/tx_rounded_background.png</filename>
            <filename>src/tx_status_call.png</filename>
            <filename>src/tx_status_check.png</filename>
            <filename>src/tx_status_fold.png</filename>
            <filename>src/tx_status_raise.png</filename>
            <filename>src/tx_table.png</filename>
            <filename>src/tx_toggle_mark_bcyan.png</filename>
            <filename>src/tx_toggle_mark_cyan_bg.png</filename>
            <filename>src/tx_toggle_mark_green_bg.png</filename>
            <filename>src/tx_toggle_mark_green.png</filename>
            <filename>src/tx_toggle_mark_yellow_bg.png</filename>
            <filename>src/tx_toggle_mark_yellow.png</filename>
            <filename>src/tx_txtbar.png</filename>
            <filename>src/tx_window_background.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
